﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
}
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

            p.click(function(event) { var word = event.target.innerHTML
                    alert(word);
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
   <table>
  <tr>
    <td><h1>Task four <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
        <p>Click one word:</p>
        <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        <span>England was</span>
        <span class="blank" id="123-456-789"></span>
        <span>to crown the new king, Edward VII, and the throne room was begin prepared for the celebration.
At the minute, it was discovered that the carpets</span>
        <span class="blank" id="223-456-789"></span>
        <span>were filthy. It was too late to send the carpets out to be 
cleaned. What else could be done in a hurry?
H.C. Booth had an idea. He was the inventor of a new kind of carpet cleaner.
The old kind of cleaner blew</span>
        <span class="blank" id="323-456-789"></span>
        <span>at the dirt.
        Dirt flew into the air and</span>
        <span class="blank" id="423-456-789"></span>
        <span>the room and then landed back in the carpet.
        It wasn't really much help. Mr Booth's cleaner sucked the dirt into</span>
        <span class="blank" id="523-456-789"></span>
        <span>.He had invented it after he put a handkerchief to his mouth, bent over
        the dirt,and sucked in his breath.Dirt</span>
        <span class="blank" id="623-456-789"></span>
        <span>the handkerchief, and Mr,Booth knew it would be a good idea to build a machine
        that pulled the dirt in-a "vacuum" cleaner.
        People didn't really like it first, though, because of its size. It was so big that it had to be</span>
       <span class="blank" id="723-456-789"></span>
        <span>a wagon by horses and parked outside a house. Long hoses were</span>
        <span class="blank" id="823-456-789"></span>
        <span>to clean the carpet. People said the machine was 
        so noisy that it frightened the other horses passing by, and they</span>
        <span class="blank" id="923-456-789"></span>
        <span>against Mr. Booth's new invention.
        But the king, hearing how the vacuum cleaner had cleaned the throne room, invited Mr. Booth to gave
        another demonstration and after that he ordered a vacuum cleaner for the place. Then everybody</span>
        <span class="blank" id="111-111-111"></span>
        <span>and it kept the inventor very busy going from house to house with the machine.
        People had tea parties when Mr. Booth was coming to clean, so that their
        guests could watch him work with his famous invention. To make things more interesting, Mr. Booth</span> 
        <span class="blank" id="222-222-222"></span>
        <span>so that everyone could observe the dirt being sucked into the tank of the machine.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
            
            <li class="word" id="423-456-780">swirled around</li>
            <li class="word" id="623-456-780">stuck to</li>
            <li class="word" id="223-456-780">underneath</li>
            <li class="word" id="923-456-780">vehemently protested the thrones</li>
            <li class="word" id="333-333-333">wanted their home vaccumed</li>
            <li class="word" id="555-555-555">cleaned the whole</li>
            <li class="word" id="123-456-780">having a ceremony</li>
            <li class="word" id="523-456-780">a bag instead</li>
            <li class="word" id="823-456-780">dragged inside place</li>
            <li class="word" id="323-456-780">puffs of air</li>
            <li class="word" id="723-456-780">hauled on</li>
            <li class="word" id="444-444-444">made his vacuum cleaner transparent</li>
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
     </td>
  </tr>
</table>
</body>
</html>
