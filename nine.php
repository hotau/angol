﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
}
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

            p.click(function(event) { var word = event.target.innerHTML
                    alert(word);
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
  <table>
  <tr>
    <td><h1>Task nine <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td> <p>Click one word:</p>
         <p id="disp"></p></td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        <span>Unless you are claustrophobic, stepping into an elevator is</span>
        <span class="blank" id="123-456-789"></span>
        <span>;many of us do it several timers a day.
        But prior to 1854, people weren't exactly lining up to use them, no matter how 
        convenient they were---cables</span>
        <span class="blank" id="223-456-789"></span>
        <span>enough that the public viewed them as death traps.
        Then, along came mechanic Elisha Otis and his miracle invention, the safety elevator.
        Thanks to his clever engineering, the cable could snap and the elevator would</span>
        <span class="blank" id="323-456-789"></span>
        <span>.However, elevators carried such a stigma that no one was willing to</span> 
        <span class="blank" id="423-456-789"></span>
        <span>to Otis's safety elevator. Sales were practically nonexistent.
        To show the public that his invention worked, 
        Otis orchestrated a stunt that would</span> 
        <span class="blank" id="523-456-789"></span>
        <span>we build work and live. In 1854,
        he constructed a 50-foot elevator at the Exhibition of the industry of
        All Nations in New York, getting P.T. Barnum himfelf arguably the most 
        famous businessman, snowman and entertainer of the times- to hype up the crowd. Otis</span>
        <span class="blank" id="623-456-789"></span>
        <span>of riding the nelevator</span> 
       <span class="blank" id="723-456-789"></span>
       <span>to the top, then cut through the
       cable that tied the elevator car to the frame. Shocked onlookers prepared
       for the inventor to plummet to a particularly ugly death-but when</span>
        <span class="blank" id="823-456-789"></span>
        <span>,the elevator dropped only a few inches.
        "All safe", he assured the crowd. Just to</span> 
        <span class="blank" id="923-456-789"></span>
        <span>, Otis repeated his demonstration
        over and over for months, proving to</span> 
        <span class="blank" id="111-111-111"></span>
        <span>that a safe elevator had finally arrived.
        Today, there are approximately 2.5 million Otis elevators in operation.
        So, the next time you step into an elevator, imagine the cable begin cleaved
        in two-and that</span>
        <span class="blank" id="222-222-222"></span>
        <span>of relief, knowing that if that happend you would be fine.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
            <li class="word" id="223-456-780">snapped frequently</li>
            <li class="word" id="923-456-780">get his point across</li>
            <li class="word" id="623-456-780">made a show</li>
            <li class="word" id="444-444-444">breathe a sigh</li>
            <li class="word" id="823-456-780">the rope snapped</li>
            <li class="word" id="423-456-780">give a chance</li>
            <li class="word" id="333-333-333">thousands of unlookers</li>
            <li class="word" id="523-456-780">change the way</li>
            <li class="word" id="323-456-780">still hold</li>
            <li class="word" id="723-456-780">all the way</li>
            <li class="word" id="123-456-780">no big deal</li>
            
            <li class="word" id="555-555-555">invent something new</li>
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
     </td>
  </tr>
</table>
</body>
</html>
