<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
}
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

            p.click(function(event) { var word = event.target.innerHTML
                    alert(word);
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
   <table>
  <tr>
    <td><h1>Task ten <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
      <p>Click one word:</p>
      <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        There is a proper time
        and place for food and it isn't in the 
        <span class="blank" id="123-456-789"></span>
        <span>, according to Oscar-Nominated actress Imelda Staunton.
         She said in an interview that she would welcome a ban on eating and drinking in theatres.
         I don't know why people can't engage in just one thing," she said."
        "I don't understand this obsession with</span>
        <span class="blank" id="223-456-789"></span>
         <span>something at every moment of the day."
        She is the latest in a line of actors, directors, 
        and even audience member to complain about increasing</span>
        <span class="blank" id="323-456-789"></span>
        <span> amoungst theatregoers.
        In the summer, producer Richard Jordan claimed his trip to see Doctor Faustus was marred by
        "possibly the worst West End audience I have ever encountered". He detailed seeing </span>
        <span class="blank" id="423-456-789"></span>
        <span>in the first half and "talking, eating and taking pictures throughout".
        Actors have described seeing fish and chip suppers eaten in the stalls- "smelt </span>
        <span class="blank" id="523-456-789"></span>
        <span>but were quite disturbing" - and being put off by the everlasting share-bags of popcorn and crips.
        One director critized a front-rower for </span>
        <span class="blank" id="623-456-789"></span>
        <span>" never-ending bag of sweets". Actor John Partridge,</span> 
       <span class="blank" id="723-456-789"></span>
        <span>in Chichago, told the BBC his most off-putting
        foodie moment came when he was playing Zach in a production of A Chorus
        Line, a role which involves sitting with the audience. "One show,
        I'm in the middle,</span> 
        <span class="blank" id="823-456-789"></span>
        <span>and a guy two seats from me, goes into his
        bag, rustle, rustle, rustle, pulls out this kind of bucket of Chinese-style chick-en wings.
        "It's not </span>
        <span class="blank" id="923-456-789"></span>
        <span>, they stink. "He turned to me: 'Do you want one?' I thught: 
        'I am immersed in my character right now. You've paid L80 to come and see this. Why would you want to</span>
        <span class="blank" id="111-111-111"></span>
        <span>?' "People actually bring lunch,</span>
        <span class="blank" id="222-222-222"></span>
        <span>in a Tupperware box. What is that? I am with Imelda. No eating in the theatre."</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
           
           
            <li class="word" id="323-456-780">eating habits</li>
            <li class="word" id="123-456-780">theatre seat</li>
            <li class="word" id="444-444-444">pre-packed</li>
            <li class="word" id="423-456-780">takeaway chicken nuggets</li> 
            <li class="word" id="223-456-780">having to eat or drink</li>
            <li class="word" id="623-456-780">chewing through</li>
            <li class="word" id="823-456-780">delivering my lines</li>
            <li class="word" id="723-456-780">currently starring</li>
            <li class="word" id="923-456-780">only the noise</li>
            <li class="word" id="555-555-555">behind the curtains</li>
            <li class="word" id="333-333-333">come and eat</li>
            <li class="word" id="523-456-780">absolutely delicious</li>
            
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
     </td>
  </tr>
</table>
</body>
</html>
