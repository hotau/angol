﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
}
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

            p.click(function(event) { var word = event.target.innerHTML
                    alert(word);
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
  <table>
  <tr>
    <td><h1>Task five <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
        <p>Click one word:</p>
         <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        <span>    
        637,903 Lego bricks were used to make a life-size Tube carriage
        on display at the world's largest Lego store, which</span> 
        <span class="blank" id="123-456-789"></span>
        <span>in Leicester Square.The store has been two years in
        development and also features models of a dragon, the Elizabeth Tower
        and Big Ben and a Royal Mail post box. The</span> 
        <span class="blank" id="223-456-789"></span>
        <span>are made from 1.7 million bricks and together weigh five toones.
        More that a third of</span>
        <span class="blank" id="323-456-789"></span>
        <span>went into the London Underground carriage,
        created in partnership with Transport for London. 
        It took 4,000 hour to make and features</span>
        <span class="blank" id="423-456-789"></span>
        <span>- a ,made of Shakespeare. The store also has a 1:15 scale model of the 
        Elizabeth Tower and Big Ben featuring</span>
        <span class="blank" id="523-456-789"></span>
        <span>and chimes with the sound of Big Ben. There are also models of a telephone box and an Underground map.
        Attraction include the world's first Lego Mosaic Maker, which</span>
        <span class="blank" id="623-456-789"></span>
        <span>a personalised Lego mosaic portrait.
        It is exclusive to the London store and scans the
        face like in a passport photo booth, and then</span>
       <span class="blank" id="723-456-789"></span>
       <span>into LEGO pixels.
       There are also play tables where children can sit and build. John Goodwin, executive vice
       president and chief financial officer of the Lego Group, said:
       "We want to inspire and develop children through</span>
        <span class="blank" id="823-456-789"></span>
        <span>-and this store is all about that."
        The flagship store is made up of two floors with jaw-dropping</span>
        <span class="blank" id="923-456-789"></span>
        <span>which took 10,000 hours to create.
        The centrepice is a beautifully intricate, two-storey chiming Big Ben with a working clock face.
        Brickly the dragon, complete with iconic London accessories of a bowler hat and black 
        umbrella, is also coiled readyto</span>
        <span class="blank" id="111-111-111"></span>
        <span>.There is also the pick-a-brick wall, where you can</span> 
        <span class="blank" id="222-222-222"></span>
        <span>to London landscapes and get help to build an original design.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
            <li class="word" id="623-456-780">allows shoppers to buy</li>
            <li class="word" id="423-456-780">an unusual passenger</li>
            <li class="word" id="223-456-780">creations on display</li>
            <li class="word" id="523-456-780">a high number of visitors</li>
            <li class="word" id="723-456-780">converts the image</li>
            <li class="word" id="923-456-780">replica models</li>
            <li class="word" id="323-456-780">those bricks</li>
            <li class="word" id="333-333-333">welcome visitors</li>
            <li class="word" id="123-456-780">opened</li>
            <li class="word" id="444-444-444">add your own flair</li>
            <li class="word" id="823-456-780">creative play experiances</li>
            <li class="word" id="555-555-555">exclusive gifts</li>
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
      </td>
  </tr>
</table>
</body>
</html>
