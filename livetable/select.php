<?php
 include "connect.php";
 
 $sql="SET @count = 0"; 
	$result=$connect->query($sql);
	$sql="UPDATE words SET id = @count:= @count + 1"; 
	$result=$connect->query($sql);
	$sql="ALTER TABLE words AUTO_INCREMENT = 1"; 
	$result=$connect->query($sql);
 
 $output = '';  
 $sql = "SELECT * FROM words ORDER BY id DESC";  
 $result = mysqli_query($connect, $sql);
 $output .= '  
      <div class="table-responsive">  
           <table class="table table-bordered">  
                <tr>  
                     <th width="5%">Id</th>  
                     <th width="15%">Angol</th>  
                     <th width="15%">Magyar</th>
                     <th width="5%">Delete</th>  
                </tr>';  
 $rows = mysqli_num_rows($result);
 if($rows > 0)  
 {  
      while($row = mysqli_fetch_array($result))  
      {  
           $output .= '  
                <tr>  
                     <td>'.$row["id"].'</td>  
                     <td class="english" data-id1="'.$row["id"].'" contenteditable>'.$row["english"].'</td>  
                     <td class="hungary" data-id2="'.$row["id"].'" contenteditable>'.$row["hungary"].'</td>
                     <td><button type="button" name="delete_btn" data-id7="'.$row["id"].'" class="btn btn-xs btn-danger btn_delete">x</button></td>  
                </tr>  
           ';  
      }  
      $output .= '  
           <tr>  
                <td></td>  
                <td id="english" contenteditable></td>  
                <td id="hungary" contenteditable></td>
                <td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
           </tr>  
      ';  
 }  
 else  
 {  
      $output .= '
				<tr>  
					<td></td>  
					<td id="english" contenteditable></td>  
					<td id="hungary" contenteditable></td>
					<td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
			   </tr>';  
 }  
 $output .= '</table>  
      </div>';  
 echo $output;
 
 
	

 
 ?>