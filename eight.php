﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
                }
            });
        
        });
             $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

            p.click(function(event) { var word = event.target.innerHTML
                    alert(word);
                    });
              });
      
      
       
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
  <tr>
    <td><h1>Task eight <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
        <p>Click one word:</p>
        <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>  
        <span>The city of Nome, Alaska, was desperately waiting for help. 1925, diphtheria, a fatal disease, was</span>
        <span class="blank" id="123-456-789"></span>.
        <span>The problem was that the city didn't have enough medicine to inoculate its citizens. Extra medicine was
        a thousand miles away. There were only some small trading posts surrounded by a huge amount of barren,</span>
        <span class="blank" id="223-456-789"></span>.
        
        
        <span>The only way to deliver the medicine would be by dogled. It would
        take two week, and that would be too long.
        Wild Bill Shanon, a rugged trapper,</span>
        
        <span class="blank" id="323-456-789"></span>
        
        <span>.If each trapping town could have a fresh dog team and driver ready,
        the serum could be run across</span> 
        <span class="blank" id="423-456-789"></span>
        <span>by relay, cutting the time down to nine days.</span>
        
        
        
       <span>With hours, Shanon was on his way from the hospital in Anchorage with a metal case that had thirty thousand doses
        of medicine inside. Through the outstanding efforts of</span> 
        <span class="blank" id="523-456-789"></span>
        <span> with their dogs collapsing into the snow at</span> 
        <span class="blank" id="623-456-789"></span>
        <span>,the medicine was passed to the medicine was passed to the fifth driver, Gunnar Kaasen, on the fifth day.
        Kassen was to pass off to one more driver, but because of a blizard, 
        he missed the trader's cabin where they</span>
       <span class="blank" id="723-456-789"></span>
       <span>
        He was alone. His lead dog, a Siberian husky named Balto, was an excelent sled dog,
        and Kassen was forced by
       </span>
        <span class="blank" id="823-456-789"></span>
        <span>
        to rely on Balto's instincts.
        In the building blizzard, at thirty degrees below zero, he couldnot see the trial. Balto pressed on for
        </span>
        <span class="blank" id="923-456-789"></span>
        <span>
        .When they reached Nome, Kasen was almost  unconsius, temporarly blinded, and badly, 
        and badly cut on his face by
        </span>
        <span class="blank" id="111-111-111"></span>
        <span>
        .He had to be chipped from the sled and the serum was a solid frozen block. Itt had been 
        deliverd in only six days, which saved all but two victims from the diphtheria that</span> 
        <span class="blank" id="222-222-222"></span>
         <span>an entire city.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
            <li class="word" id="423-456-780">the thousand miles</li>
            <li class="word" id="123-456-780">threating the city</li>
            <li class="word" id="623-456-780">each meeting point</li>
            <li class="word" id="223-456-780">snow-covered wilderness</li>
            <li class="word" id="823-456-780">the gigantic snowstorm</li>
            <li class="word" id="523-456-780">four relay teams miles</li>
            <li class="word" id="923-456-780">the next fifty-three</li>
            <li class="word" id="444-444-444">could have wiped out</li>
            <li class="word" id="323-456-780">thought of a solution</li>
            <li class="word" id="333-333-333">ice particles</li>
            <li class="word" id="723-456-780">were to meet</li>
            <li class="word" id="555-555-555">a team of sled dogs</li>
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
       </td>
  </tr>
</table>
</body>
</html>
