<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
}
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

           p.click(function(event) {
                     var id=event.target.innerHTML
                     $.ajax({
                    url:"name.php ",
                    type:"POST",
                    data:{
                        item_id:id,
                    },
                    success:function(response) {
                    document.getElementById("disp").innerHTML = response;
                    }
                        });
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
    <table>
  <tr>
    
    <td><h1>Task three <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
        <p>Click one word:</p>
        <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        <span>    
        Book lovers and theatregoers alike, consider planing your</span>
        <span class="blank" id="123-456-789"></span>
        <span>to Argentina to check out a spectacular,</span>   
        <span class="blank" id="223-456-789"></span>
        <span>bookstore built within the old Grand Slendid Theatre in Buenos Aires.
        Located in the neighbourhood of Recoleta, the structure originally opened as a theatre in 
        May 1919 with all kind of performances</span>
        <span class="blank" id="323-456-789"></span>
        <span>.It had a</span>
        <span class="blank" id="423-456-789"></span>
        <span>as a cinema during the late 1920's.
        It sat 1,050 people, hosted ballets, operas tango singers, and later, "talkie" films. In 2000,
        architect Fernando Manzone converted the space into the El Ateneo Grand     
        Splendid bookstore, which now serves as the group's</span>
        <span class="blank" id="523-456-789"></span>
        <span>with over one million visitors each year.
        The space where the audience once sat is filled with bookshelves holding over 120,000 books.
        Still, signs of the building's former life are unmistakable. The dim, theatrical lighting 
        remains the same as done much of the</span>
        <span class="blank" id="623-456-789"></span>
        <span>, including the balconies, sculptures, ornate
        carvings, the thick burgundy stage curtains and the</span>
       <span class="blank" id="723-456-789"></span>
       <span>painted
        by Italian artist Nazareno Orlandi. Theatre boxes serve as cosy reading  nooks
        and lifelong acting dreams can be secretly fulfilled as you
        step onto what was once the</span>
        <span class="blank" id="823-456-789"></span>
        <span>to visit the cafe.
        The bookstore is appealing for both the classic 1920's theatre
        feel and elaborate décor, as well as the</span>
        <span class="blank" id="923-456-789"></span>
        <span>of books that shoppers can peruse.
        In keeping with the initial inception as a</span>
        <span class="blank" id="111-111-111"></span>
        
        <span>
        , a pianist plays live music onstage in the afternoons.
        What a fantastic way to spend a day, flipping through 
        pages and enjoying the awesome
        acoustics in the same place where famous tango artists once took
        the stage. Around 3,000 people visit the bookstore every day.
        It's</span>
        <span class="blank" id="222-222-222"></span>
        
        <span>As well as a theatre geek's.</span>
        </p>
        </div>
        <ul class="words">
            <li class="word" id="823-456-780">main stage</li>
            <li class="word" id="123-456-780">next vacation</li>
            <li class="word" id="333-333-333">performance arena</li>
            <li class="word" id="223-456-780">one-of-a-kind</li>
            <li class="word" id="723-456-780">mural on the ceiling</li>
            <li class="word" id="523-456-780">flagship store</li>
            <li class="word" id="323-456-780">gracing the stage</li>
            <li class="word" id="623-456-780">original interior</li>
            <li class="word" id="423-456-780">brief stint</li>
            <li class="word" id="444-444-444">bookworm's dream</li>
            <li class="word" id="923-456-780">huge amount</li>
            <li class="word" id="555-555-555">entrance to the building</li>
        </ul>
        
        <div class="actions">
            <a href="three.php" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
    </td>
  </tr>
</table>
</body>
</html>
