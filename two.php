<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
            }
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

           p.click(function(event) {
                     var id=event.target.innerHTML
                     $.ajax({
                    url:"name.php ",
                    type:"POST",
                    data:{
                        item_id:id,
                    },
                    success:function(response) {
                    document.getElementById("disp").innerHTML = response;
                    }
                        });
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
    
    
<table>
  <tr>
    <td><h1>Task two <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
        <p>Click one word:</p>
        <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        <span>We're all family with the all_purpose vehicles</span>
        <span class="blank" id="123-456-789"></span>
        <span>But very few_of_us know that teir design began as a contest sponsored by the United States government.
        A four-wheel-drive vehicle that was practical,</span>
        <span class="blank" id="223-456-789"></span>
        <span>,and could carry at_least a quarter-ton load was needed  for the army troops of the Second World War.
        But the vehicle could not weigh more than 2,160 pounds. Three automakers submitted designs.
        One of the companies had a problem with the weight- they were 250 pounds</span>
        <span class="blank" id="323-456-789"></span>
        <span>At the last minute, they revised their vehicle to see how thely could</span>
        <span class="blank" id="423-456-789"></span>
        <span>The revisions were so close that only one coat of paint could be put on the prototype.
        The second coat would have made the vehicle too heavy.
        In the end, the army decided to use a Jeep that</span>
        <span class="blank" id="523-456-789"></span>
        <span>of three designs from the three different companies.
        The many abilities of three designs from the three different companies.
        The many abilities of this special vehicles may have helped lead to the Jeep name.
        The vehicle</span>
        <span class="blank" id="623-456-789"></span>
        <span>as a fact-finding spy car by the army in 1940, and it arrived with the letters GP painted on its side.
        The letters stood for "General Purpose," and many people claim that the name</span>
       <span class="blank" id="723-456-789"></span>
       <span>of how the initials were said. Others, however, have a different opinion and they</span>
        <span class="blank" id="823-456-789"></span>
        <span>in the Prepeye stories whose name was Eugene the Jeep. Appearing in comic strips during the 
        late 1930s, he was a little creature who ate orchids, solved problems, and could</span>
        <span class="blank" id="923-456-789"></span>
        <span>.Whichever of the two possibilities really gave the Jeep its name, one thing was certain
        - it was indeed used for</span>
        <span class="blank" id="111-111-111"></span>
        <span>of purposes. During the war, there was a Jeep that was completely
        buried by sand in a desert, and another that</span> 
        <span class="blank" id="222-222-222"></span>
        <span>tied to a submarine but both were still very much drivable afterward.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
           
            <li class="word" id="123-456-780">know_as_Jeeps</li>
            <li class="word" id="623-456-780">was_first_used</li>
             <li class="word" id="444-444-444">travelled_underwater</li>
            <li class="word" id="523-456-780">was_the_combination</li>
            <li class="word" id="723-456-780">was_a_shortened_version</li>
             <li class="word" id="423-456-780">make_it_a_bit_lighter</li>
              <li class="word" id="223-456-780">easy_to_drive</li>
              <li class="word" id="823-456-780">point_to_the_character</li>
             <li class="word" id="323-456-780">over_the_limit</li>
             <li class="word" id="333-333-333">a_large_variety_of_purposes</li>
            <li class="word" id="923-456-780">do_almost_anything</li>
            <li class="word" id="555-555-555">designed_by_the_engineers</li>
        </ul>
        
        <div class="actions">
            <a href="two.php" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
    </td>
  </tr>
</table>
</body>
</html>
