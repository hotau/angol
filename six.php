<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
}
            });
        });
        
         $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

            p.click(function(event) { var word = event.target.innerHTML
                    alert(word);
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
  <table>
  <tr>
    <td><h1>Task six <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  </tr>
  <tr>
    <td>
        <p>Click one word:</p>
        <p id="disp"></p>
    </td>
  </tr>
  <tr>
    <td>
    <div class="jdropwords">
        <div class="blanks">
        <p>
        Bats are often seen as scary or
        <span class="blank" id="123-456-789"></span>
        <span>
        of the night,commonly found in Halloween themed decorations, or haunted houses.
        Even the animal-loving Ace Ventura didn't like them. But bats aren't scary-they're</span>
        <span class="blank" id="223-456-789"></span>
        <span>.What's cuter than an orphaned baby bat swaddled in baby blankets?
        A veterinary clinic full of orphaned baby  bats swaddled in</span>
        <span class="blank" id="323-456-789"></span>
        <span>, that's what. The Australian Bat Clinic & Wildlife Trauma Center in easter Australia's
        Gold Coast Hinterland takes in baby bats (called 'pups') when their mothers have died,
        and also treats adult bats that are afflicted by mites and other</span>
        <span class="blank" id="423-456-789"></span>
        <span>.The baby blankets they wrap the young bat pups in aren't</span> 
        <span class="blank" id="523-456-789"></span>
        <span>- is the baby bats have been orphaned, they lack</span>
        <span class="blank" id="623-456-789"></span>
        <span>of their mothers' wings, so these blankets are the next best thing.
        The centre was founded by American-born Terry Wimberle, and aims to help</span>
       <span class="blank" id="723-456-789"></span>
       <span>,which can sometimes and up orphaned or in need of immediate care after begin 
        plagued with mites, suffering in</span>
        <span class="blank" id="823-456-789"></span>
        <span>,or having run-ins with barbed wire, among other terrible things.
        The centre is very active on the Internet, and posts plenty of bat photos for the public to enjoy. 
        The images range from babies wrapped in blankets, to slightly older bats wearing</span>
        <span class="blank" id="923-456-789"></span>
        <span>.Baby bats love to be held; they cling to their mothers, who then</span>
        <span class="blank" id="111-111-111"></span>
        <span>them. When bottle-fed, the tiny mammals look like furry infants. All bats start with</span> 
        <span class="blank" id="222-222-222"></span>
        <span>, but move on to insects or fruit,
        depending on the type of bat. Flying foxes-the largest 
        type of bat in Australia-exclusively eat fruits like mango or lemon.
        They chew the fruit until they've extracted all the juice, and then split out the pulp.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
            <li class="word" id="723-456-780">these flying mammals</li>
            <li class="word" id="323-456-780">baby blankets</li>
            <li class="word" id="123-456-780">creepy creatures</li>
            <li class="word" id="423-456-780">bat diseases</li>
            <li class="word" id="823-456-780">extreme heat</li>
            <li class="word" id="333-333-333">wrap their wings around</li>
            <li class="word" id="523-456-780">just for looks</li>
            <li class="word" id="223-456-780">downright adorable</li>
            <li class="word" id="923-456-780">some trendy vests</li>
            <li class="word" id="444-444-444">a steady diet of milk</li>
            <li class="word" id="623-456-780">the warm embrace</li>
            <li class="word" id="555-555-555">a fenced enclosure</li>
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
      </td>
  </tr>
</table>
</body>
</html>
