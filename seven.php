﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
			}
            });
        });

			$(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

           p.click(function(event) {
                     var id=event.target.innerHTML
                     $.ajax({
                    url:"name.php ",
                    type:"POST",
                    data:{
                        item_id:id,
                    },
                    success:function(response) {
                    document.getElementById("disp").innerHTML = response;
                    }
                        });
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
   <h1>Task seven <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1></td>
  <p>Click one word:</p>
    <p id="disp"></p>
  
	<div class="jdropwords">
        <div class="blanks">
        <p>
        <span>Drivers in the Wadden Sea found a fancy</span>
        <span class="blank" id="123-456-789"></span>
        <span>gown partially buried in the sand. The grown is
        well-preserved and may be one of the most important artefacts ever brought up from the sea floor. 
        The dress was recovered off the Dutch island of Texel, which was, for a time</span>
        <span class="blank" id="223-456-789"></span>
        <span>.Unfortunately for traders, Texel's location also made it a prime site for shipwrecks.
        Many of those wrecks have been washed further out to sea, but some remain in the
        rekative shallows surrounding the island. Drives generally
        avoid disturbing them, instead waiting for the ocean to reveal the rotting wrecks. Two years ago, in</span>
        <span class="blank" id="323-456-789"></span>
        <span>from the 1600s drivers found</span>
        <span class="blank" id="423-456-789"></span>
        <span>and brought it back to the surface.
        They opened the package in the open air and realized they had found the contents of</span>
        <span class="blank" id="523-456-789"></span>
        <span>, and that someone must have been pretty rich. There were silk knee socks and a
        jacket, as well as a silk bodice embroidered in silver and gold. But</span>
        <span class="blank" id="623-456-789"></span>
        <span>was a damask grown with a high collar and ruffled 
        sleeves-the kind of thing noblewomen or royalty might wear</span>
       <span class="blank" id="723-456-789"></span>
       <span>. For the gown, anyway, the  shipwreck had been a blessing; on land, if it had been exposed to air and moths,
        it would be in much worse shape than</span>
        <span class="blank" id="823-456-789"></span>
        <span>. Besides that,</span> 
        <span class="blank" id="923-456-789"></span>
        <span>also contained leather-bound books. One of those books bears the coat of arms of King Charles 
        I, which suggests that the ship's passengers may have belonged to the royal house of Stuart.
        The grow and other finds</span>
        <span class="blank" id="111-111-111"></span>
        <span>are currently on display at
        Texel's belightful museum, and will return there</span>
        <span class="blank" id="222-222-222"></span>
        <span>after they are examined and treated by conservators.</span>
        </p>
        </div>
        
        
        
        
        <ul class="words">
            <li class="word" id="523-456-780">someone's wardrobe</li>
            <li class="word" id="123-456-780">seventeenth-century</li>
            <li class="word" id="723-456-780">around the house</li>
            <li class="word" id="444-444-444">permanently</li>
            <li class="word" id="223-456-780">an important centre</li>
            <li class="word" id="423-456-780">a mysterious package</li>
            <li class="word" id="923-456-780">the wreck site</li>
            <li class="word" id="623-456-780">the most impressive</li>
            <li class="word" id="333-333-333">from the shipwreck</li>
            <li class="word" id="323-456-780">a merchant ship of trade</li>
            <li class="word" id="823-456-780">it is today</li>
            <li class="word" id="555-555-555">grant value</li>
        </ul>
        
        <div class="actions">
            <a href="#" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
</body>
</html>
