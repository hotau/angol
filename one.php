﻿ 
<html>
<head>
    <meta charset="UTF-8">
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/jdropwords.js"></script>
    <script>
        $(function(){
            $('.jdropwords').jDropWords({
                answers : {
                    "123-456-789" : "123-456-780",
                    "223-456-789" : "223-456-780",
                    "323-456-789" : "323-456-780",
                    "423-456-789" : "423-456-780",
                    "523-456-789" : "523-456-780",
                    "623-456-789" : "623-456-780",
                    "723-456-789" : "723-456-780",
                    "823-456-789" : "823-456-780",
                    "923-456-789" : "923-456-780",
                    "111-111-111" : "333-333-333",
                    "222-222-222" : "444-444-444"
                }
            });
        });
        
       $(function(){
                 var p = $('span');
            p.html(function(index, oldHtml) {
            return oldHtml.replace(/\b(\w+?)\b/g, '<span class="word">$1</span>')
            });

           p.click(function(event) {
                     var id=event.target.innerHTML
                     $.ajax({
                    url:"name.php ",
                    type:"POST",
                    data:{
                        item_id:id,
                    },
                    success:function(response) {
                    document.getElementById("disp").innerHTML = response;
                    }
                        });
                    });
              });
    </script>
    <link type="text/css" rel="stylesheet" href="css/jdropwords.css" media="all" />
</head>
<body>
    
   
    <h1>Task one <a href="livetable/index.php" target="_blank"><button type="button" class="btn btn-primary"> Szótár</button></a></h1>
  <td>
    <p>Click one word:</p>
    <p id="disp"></p>
  </td>
  
        <div class="jdropwords">
        <div class="blanks">
        <p>
        <span>If a teenager's room looks_like a natural_disaster, should parents simply</span>
        <span class="blank" id="123-456-789"></span>
        <span>
        or insist their child clean_it_up? Experts' views differ.
        An  author of several books about teens recommends that parents set
        </span>
        <span class="blank" id="223-456-789"></span>
        <span>
        so the room is neither a five hazard no a hygienic hazard but allow some freedom too.
        For_example one rule migth_be that the teens don't eat in the room
        </span>
        <span class="blank" id="323-456-789"></span>
        <span>
        so there's no dry pizza left on the bed for weeks.
        Parents sould have teens take responsibillity for
        </span>
        <span class="blank" id="423-456-789"></span>
        <span>
        of their clothes and wash them. And is they don't do that,
        they'll have to wear dirty clothes which is
        </span>
        <span class="blank" id="523-456-789"></span>
        <span>
        of their negligence. Beyond that, if the room is just messy, let the teenager sit in the mess and
        shut_the_door, she says. She recommends avoiding
       </span>
       <span class="blank" id="623-456-789"></span>
       <span>
       over the room. If you let the room go, then when you say "No" in another area that you consider
       important, they are
       </span>
       <span class="blank" id="723-456-789"></span>
       <span>
       to hear you. But a psychologist says parents can expect teen clean their room at_least once a week.
        Even the worst room is not going to get that bad in a week
        in terms of insect crawling_out from under the door.
        She thinks
        </span>
        <span class="blank" id="823-456-789"></span>
        <span>
        to get teens to clean their rooms
        is to devise
        </span>
        <span class="blank" id="923-456-789"></span>
        <span>
        .Tie room-cleaning to a teen's allowance, for_example : If you expect the room to_be_cleaned_up
        by Thursday at 9 p.m., and it's done, pay the allowance. If parents don't care
        if their teen's room is messy, they can given the child
        </span>
        <span class="blank" id="111-111-111"></span>
        <span>
        such_as cooking for the family once a week or cleaning the table three times a
        week or feeding the dog. It's healthy for kids to participate in the family in some
        helpful way. And cleaning their rooms builds habits that teach
         </span>
         <span class="blank" id="222-222-222">
        </p>
        </div>
        <ul class="words">
            <li class="word" id="323-456-780">overnight</li>
            <li class="word" id="523-456-780">the_natural_consequence</li>
            <li class="word" id="123-456-780">shut_the_door</li>
            <li class="word" id="623-456-780">power_struggles</li>
            <li class="word" id="823-456-780">the_best_way</li>
            <li class="word" id="333-333-333">other_responsibilities</li>
            <li class="word" id="423-456-780">keeping_track</li>
            <li class="word" id="223-456-780">some_general_limits</li>
            <li class="word" id="555-555-555">some_difficulties</li>
            <li class="word" id="723-456-780">more_likely</li>
            <li class="word" id="923-456-780">an_intensive_plan</li>
            <li class="word" id="444-444-444">organisation_and_order</li>
        </ul>
        <div class="actions">
            <a href="one.php" class="button reset">Reset</a>
            <a href="#" class="button submit">Submit</a>
        </div>
    </div>
</body>
</html>
